I loved playing [Hempuli's Baba Is You](https://hempuli.com/baba/). This project is my attempt to recreate some of the game's fundamental features and beginning levels for the web. If you find this interesting, go buy the game!

# Install

```
$ npm install
```

# Run

```
$ npm run client-build
$ npm run server-prod
```

Visit `http://localhost:3000` to try out.
