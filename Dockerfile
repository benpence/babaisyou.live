FROM node

WORKDIR /root
ADD ./ /root

RUN npm install
RUN npm run client-build

ENTRYPOINT npm run server-prod
