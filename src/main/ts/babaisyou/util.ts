export const range = (start: number, end: number): Array<number> => {
  const arr = Array(end - start)

  for (let i = start; i < end; i++) {
    arr[i - start] = i
  }

  return arr
}

export const capitalize = (s: string): string => {
  return s[0].toUpperCase() + s.slice(1).toLowerCase()
}