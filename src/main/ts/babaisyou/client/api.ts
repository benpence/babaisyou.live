import * as path from "path"
import * as api from "babaisyou/api"
import * as game from "babaisyou/game"

export class RemoteApi implements api.Client {
  apiBaseUrl: string

  constructor(apiBaseUrl: string) {
    this.apiBaseUrl = apiBaseUrl
  }

  public getLevel = (levelNumber: number): Promise<game.Grid> => {
    return fetch(`${this.apiBaseUrl}/${path.join(api.levelPath, levelNumber.toString())}`)
      .then(response => response.json())
      .then(api.jsonToGrid)
      .catch(err => Promise.reject(new Error(`Unable to get level ${levelNumber}: ${err}`)))
  }
}

export class LocalApi implements api.Client {
  levels: Array<game.Grid>

  constructor(levels: Array<game.Grid>) {
    this.levels = levels
  }

  public getLevel = (levelNumber: number): Promise<game.Grid> => {
    if (levelNumber < 0 || levelNumber > this.levels.length - 1) {
      return Promise.reject(new Error(`The local API only has ${this.levels.length} levels. Asked for level index ${levelNumber}`))
    }

    return Promise.resolve(this.levels[levelNumber])
  }
}