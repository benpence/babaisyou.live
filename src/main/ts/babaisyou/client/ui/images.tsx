import * as game from "babaisyou/game"
import * as React from "react"

export interface Render {
  ofSquare: (square: game.Square) => React.ReactElement
}

const squarePieces = (square: game.Square): Array<game.Piece> => {
  let pieces: Array<game.Piece> = Array.from(square.subjects)

  if (square.word !== null) {
    pieces.push(square.word)
  }

  return pieces
}

export class EmojiRender implements Render {
  subjectEmojiMap: Map<game.Subject, string>
  wordEmojiMap: Map<game.WordType, string>
  empty: string

  constructor(subjectEmojiMap: Map<game.Subject, string>, wordEmojiMap: Map<game.WordType, string>, empty: string) {
    for (let subject of Object.values(game.Subject)) {
      if (!subjectEmojiMap.has(subject)) throw EmojiRender.err(subject)
      if (!wordEmojiMap.has(subject)) throw EmojiRender.err(subject, true)
    }

    for (let desc of Object.values(game.Desc)) {
      if (!wordEmojiMap.has(desc)) throw EmojiRender.err(desc, true)
    }

    for (let connector of Object.values(game.Desc)) {
      if (!wordEmojiMap.has(connector)) throw EmojiRender.err(connector, true)
    }

    this.subjectEmojiMap = subjectEmojiMap
    this.wordEmojiMap = wordEmojiMap
    this.empty = empty
  }

  private static err = (piece: game.WordType, isWord?: boolean): string => {
    const word = isWord ? " word" : ""
    return `emojiMap is missing \"${piece}\"${word}.`
  }

  public ofSquare = (square: game.Square): React.ReactElement => {
    const emojis = squarePieces(square).map(piece => {
      if (game.pieceIsSubject(piece)) {
        const className = `grid-image emoji-subject ${piece.toLowerCase()}`
        return <span className={className}>{this.subjectEmojiMap.get(piece)}</span>
      } else {
        const typeClassName = game.Word.isDesc(piece.content) ? "desc" : "subject"
        const className = `grid-image emoji-word ${piece.content.toLowerCase()} ${typeClassName}`
        return <span className={className}>{this.wordEmojiMap.get(piece.content)}</span>
      }
    })

    return emojis.length == 0
      ? <span className="grid-image emoji-empty empty">{this.empty}</span>
      : <>{emojis}</>
  }
}

export class RemoteRender implements Render {
  imageDir: string

  constructor(imageDir: string) {
    this.imageDir = imageDir
  }

  public ofSquare = (square: game.Square): React.ReactElement => {
    return <>{squarePieces(square).map(this.piece)}</>
  }

  piece = (piece: game.Piece): React.ReactElement => {
    const imagePath = game.pieceIsSubject(piece)
      ? `${this.imageDir}/${piece.toLowerCase()}.png`
      : `${this.imageDir}/word_${piece.content.toLowerCase()}.png`

    return <img src={imagePath}></img>
  }
}