import * as React from "react"
import * as game from "babaisyou/game"
import * as api from "babaisyou/api"
import * as images from "babaisyou/client/ui/images"
import * as util from "babaisyou/util"

type UIAction = "NextLevel" | "Undo" | game.Action
const uiActionIsGameAction = (action: UIAction): action is game.Action => {
  return action !== "NextLevel" && action !== "Undo" && action !== null
}

const eventToUIAction = (event: React.KeyboardEvent): UIAction | null => {
  switch (event.key) {
    case " ": return "NextLevel"
    case "Backspace":
    case "u":
      return "Undo"
    case "r": return "Restart"
    case "ArrowUp":
    case "w":
      return game.Move.Up
    case "ArrowDown":
    case "s":
      return game.Move.Down
    case "ArrowLeft":
    case "a":
      return game.Move.Left
    case "ArrowRight":
    case "d":
      return game.Move.Right
    default: return null
  }
}

export const App = (props: { levelOne: game.Grid, images: images.Render, client: api.Client }) => {
  const [levelNumber, setLevelNumber] = React.useState(0)
  const [ongoingGame, setGame] = React.useState(game.Game.startingAtLevel(props.levelOne))
  const [history, setHistory] = React.useState([props.levelOne])

  const handleUiAction = (action: UIAction): void => {
    if (action === "NextLevel" && ongoingGame.status() === "Win") {
      setLevelNumber(levelNumber + 1)
      props.client.getLevel(levelNumber + 1)
        .then(level => {
          setHistory([level])

          setGame(game.Game.startingAtLevel(level))
        })

    } else if (action === "Undo" && history.length > 1) {
      const previousGrid = history[history.length - 1]
      setHistory(history.slice(0, history.length - 1))

      setGame(ongoingGame.withCurrentGrid(previousGrid))

    } else if (uiActionIsGameAction(action)) {
      const [newGame, actionPerformed] = ongoingGame.performAction(action)

      if (action === "Restart") {
        setHistory([newGame.grid])

        setGame(newGame)
      } else if (actionPerformed) {
        setHistory([...history, ongoingGame.grid])

        setGame(newGame)
      }
    }
  }

  const focus = (component: HTMLOrSVGElement): void => {
    if (component !== null) {
      component.focus()
    }
  }

  return (
    <div id="app">
      <h1 id="title">Baba Is You (<a href="https://hempuli.com/baba/">original game</a>) (<a href="https://gitlab.com/benpence/babaisyou.live">source</a>)</h1>
      <div
        className="game"
        onKeyDown={event => handleUiAction(eventToUIAction(event))}
        tabIndex={0}
        ref={focus}
      >
        <Game ongoingGame={ongoingGame} images={props.images} />
      </div>
    </div>
  )
}

const Game = (props: { ongoingGame: game.Game, images: images.Render }) => {
  return (
    <>
      <Grid grid={props.ongoingGame.grid} images={props.images} />
      <Sidebar ongoingGame={props.ongoingGame} />
    </>
  )
}

const Grid = (props: { grid: game.Grid, images: images.Render }) => {
  return (
    <table className="grid">
      <tbody>
        {util.range(0, props.grid.height()).map(row =>
          <tr className="grid-row">
            {util.range(0, props.grid.width()).map(col => {
              const square = props.grid.get(new game.Coord(row, col))

              return <Square square={square} images={props.images} />
            })}
          </tr>
        )}
      </tbody>
    </table>
  )
}

const Square = (props: { square: game.Square, images: images.Render }) => {
  return (
    <td className="grid-square">
      {props.images.ofSquare(props.square)}
    </td>
  )
}

const Sidebar = (props: { ongoingGame: game.Game }) => {
  return (
    <div id="sidebar">
      <Status status={props.ongoingGame.status()} />
      <Controls />
    </div>
  )
}

const Status = (props: { status: string }) => {
  return (
    <div className="status">Status: <span id="status-word">{props.status.toUpperCase()}</span></div>
  )
}

const Controls = () => {
  return (
    <div className="controls">
      <ul>
        <li className="controls-arrows">
          <span className="key" id="up-arrow">↑</span>
          <span className="key" id="left-arrow">←</span>
          <span className="key" id="down-arrow">↓</span>
          <span className="key" id="right-arrow">→</span>
          <span className="controls-explanation">Move anything that IS YOU</span>
        </li>
        <li className="controls-undo">
          <span className="key" id="undo">Backspace</span>
          <span className="controls-explanation">Undo the move you just made</span>
        </li>
        <li className="controls-restart">
          <span className="key" id="restart">r</span>
          <span className="controls-explanation">Restart level</span>
        </li>
        <li className="controls-next-level">
          <span className="key" id="next-level">Space</span>
          <span className="controls-explanation">Move on to next level (when you WIN)</span>
        </li>
      </ul>
    </div>
  )
}