import * as React from "react"
import * as ReactDOM from "react-dom"

import * as game from "babaisyou/game"
import * as api from "babaisyou/client/api"
import * as remote from "babaisyou/client/ui/images"
import * as view from "babaisyou/client/ui/view"

const subject = (s: game.Subject): game.Square => {
  const set = new Set
  set.add(s)
  return { word: null, subjects: set }
}

const word = (word: game.WordType) => {
  return { word: new game.Word(word), subjects: new Set }
}

const empty = { word: null, subjects: new Set }

// const client = new api.LocalApi([
//   new game.Grid([
//     [subject(game.Subject.Baba), empty, empty],
//     [empty, word(game.Subject.Flag), empty],
//     [empty, empty, subject(game.Subject.Flag)],
//     [empty, word(game.Connector.Is), word(game.Desc.Win)],
//     [word(game.Subject.Baba), word(game.Connector.Is), word(game.Desc.You)],
//   ]),
//   new game.Grid([
//     [subject(game.Subject.Baba), empty, empty],
//     [subject(game.Subject.Flag), subject(game.Subject.Wall), subject(game.Subject.Wall)],
//     [word(game.Subject.Flag), word(game.Connector.Is), word(game.Desc.Win)],
//     [word(game.Subject.Wall), word(game.Connector.Is), word(game.Desc.Stop)],
//     [word(game.Subject.Baba), word(game.Connector.Is), word(game.Desc.You)],
//   ]),
// ])

const client = new api.RemoteApi("/api/")

// const images = new remote.RemoteImages("/static/img")

const render = new remote.EmojiRender(
  new Map([
    [game.Subject.Baba, "🐏"],
    [game.Subject.Bush, "🥦"],
    [game.Subject.Column, "💈"],
    [game.Subject.Flag, "🏴"],
    [game.Subject.Key, "🔑️"],
    [game.Subject.Rock, "⛰️"],
    [game.Subject.Wall, "🧱"],
    [game.Subject.Water, "🌊"],
    [game.Subject.Grass, "🌱"],
    [game.Subject.Skull, "💀"],
    [game.Subject.Lava, "🌋"],
    [game.Subject.Violet, "⚘"],
  ]),
  new Map<game.WordType, string>([
    [game.Subject.Baba, "BABA"],
    [game.Subject.Bush, "BUSH"],
    [game.Subject.Column, "COLUMN"],
    [game.Subject.Flag, "FLAG"],
    [game.Subject.Key, "KEY"],
    [game.Subject.Rock, "ROCK"],
    [game.Subject.Wall, "WALL"],
    [game.Subject.Water, "WATER"],
    [game.Subject.Grass, "GRASS"],
    [game.Subject.Skull, "SKULL"],
    [game.Subject.Lava, "LAVA"],
    [game.Subject.Violet, "VIOLET"],
    [game.Desc.You, "YOU"],
    [game.Desc.Win, "WIN"],
    [game.Desc.Defeat, "DEFEAT"],
    [game.Desc.Float, "FLOAT"],
    [game.Desc.Sink, "SINK"],
    [game.Desc.Stop, "STOP"],
    [game.Desc.Push, "PUSH"],
    [game.Desc.Hot, "HOT"],
    [game.Desc.Melt, "MELT"],
    [game.Connector.Is, "IS"],
  ]),
  " "
)

// TODO: Restructure this with error message UI element?
client.getLevel(0).then(level =>
  ReactDOM.render(<view.App levelOne={level} images={render} client={client} />, document.getElementById("root"))
)