import * as game from "babaisyou/game"
import * as util from "babaisyou/util"

export interface Client {
  getLevel: (levelNumber: number) => Promise<game.Grid>,
}

export const levelPath = "level"

export type Level = Array<Array<Square>>
export interface Square {
  word?: string,
  subjects: Array<string>
}

export const gridToJson = (grid: game.Grid): Level => {
  const level = []

  for (let row of util.range(0, grid.height())) {
    const levelRow = []

    for (let col of util.range(0, grid.width())) {
      const square = grid.get(new game.Coord(row, col))
      levelRow.push({
        word: square.word ? square.word.content : null,
        subjects: Array.from(square.subjects)
      })
    }

    level.push(levelRow)
  }

  return level
}

export const jsonToGrid = (json: Level): game.Grid => {
  const level = []

  for (let row of util.range(0, json.length)) {
    const levelRow = []

    for (let col of util.range(0, json[row].length)) {
      const inputSquare = json[row][col]
      const word = inputSquare.word ? new game.Word((<game.WordType>inputSquare.word)) : null

      const square = { word, subjects: new Set(inputSquare.subjects) }

      levelRow.push(square)
    }

    level.push(levelRow)
  }

  return new game.Grid(level)
}