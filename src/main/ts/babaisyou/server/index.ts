import * as express from 'express'
import * as path from 'path'
import * as api from 'babaisyou/server/api'
import * as route from 'babaisyou/server/route'

const hostname = process.argv[2];
const port = Number.parseInt(process.argv[3]);
const staticDir = path.join(process.cwd(), process.argv[4]);

const app: express.Application = express();

app.use('/static', route.statics(staticDir))

app.use('/api', route.apiMethods(
  new api.FileApiService(
    path.join(staticDir, "level"),
    api.parsePlaintextGrid
  )
))

app.use('/', route.root(staticDir))

app.listen(port, hostname, () => {
  console.log(`Listening on ${hostname}:${port}`);
})