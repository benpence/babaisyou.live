import * as path from "path"
import * as api from "babaisyou/api"
import * as game from "babaisyou/game"
import * as util from "babaisyou/util"
import * as serverUtil from "babaisyou/server/util"

export type LevelParser = (levelContents: string) => game.Grid

/**
 *    baba   |         |         
 *    FLAG   |         |  flag   
 *           |   IS    |   WIN   
 *    BABA   |   IS    |   YOU   
 */
export const parsePlaintextGrid = (plaintext: string): game.Grid => {
  const squares = []

  for (let line of plaintext.split("\n")) {
    // Empty line
    if (line === "") {
      continue
    }

    const row = []

    for (let cell of line.split("|")) {
      let word = null
      const subjects = new Set

      for (let piece of cell.split(/\s+/)) {
        if (piece === "") {
          continue
        }

        const pieceString = util.capitalize(piece)

        // Word
        if (piece.charAt(0) === piece.charAt(0).toUpperCase()) {
          if (Object.values(game.Subject).includes(pieceString)) {
            word = new game.Word(game.Subject[pieceString])
          } else if (Object.values(game.Desc).includes(pieceString)) {
            word = new game.Word(game.Desc[pieceString])
          } else {
            word = new game.Word(game.Connector[pieceString])
          }

          // Subject
        } else if (piece.charAt(0) === piece.charAt(0).toLowerCase()) {
          subjects.add(game.Subject[pieceString])
        }
      }

      row.push({ word, subjects })
    }

    squares.push(row)
  }

  return new game.Grid(squares)
}

export class FileApiService implements api.Client {
  levelDir: string
  parser: LevelParser
  cache: Map<number, game.Grid>

  constructor(levelDir: string, parser: LevelParser) {
    this.levelDir = levelDir
    this.parser = parser
    this.cache = new Map
  }

  getLevel = (levelNumber: number): Promise<game.Grid> => {
    if (this.cache.has(levelNumber)) {
      return Promise.resolve(this.cache.get(levelNumber))
    } else {
      // TODO: Add support for checking if level doesn't exist
      return serverUtil
        .readFile(path.join(this.levelDir, `${levelNumber}.level`))
        .then(this.parser)
        .then(grid => {
          this.cache.set(levelNumber, grid)
          return grid
        })
    }
  }
}