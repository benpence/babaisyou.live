import * as express from 'express'
import * as path from 'path'
import * as serveStatic from 'serve-static'
import * as api from 'babaisyou/api'

const staticOptions: serveStatic.ServeStaticOptions = {
  index: false,
}

export const statics = (staticDir: string): express.Handler => {
  return express.static(staticDir)
}

export const apiMethods = (client: api.Client): express.Handler => {
  const router = express.Router()

  router.get('/level/:levelNumber(\\d+)', async (req, res) => {
    const level = await client
      .getLevel(req.params.levelNumber)
      .then(api.gridToJson)

    res.json(level)
  })

  return router
}

export function root(staticDir: string): express.Handler {
  return (req: express.Request, res: express.Response) => {
    res.sendFile(path.join(staticDir, "index.html"))
  }
}