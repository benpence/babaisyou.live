export enum Move {
  Up = "Up",
  Down = "Down",
  Left = "Left",
  Right = "Right",
}

export class Coord {
  public row: number
  public col: number

  constructor(row: number, col: number) {
    this.row = row
    this.col = col
  }

  static deltas = new Map([
    [Move.Up, new Coord(-1, 0)],
    [Move.Down, new Coord(1, 0)],
    [Move.Left, new Coord(0, -1)],
    [Move.Right, new Coord(0, 1)],
  ])

  static fromMove = (move: Move): Coord => {
    return Coord.deltas.get(move)
  }

  public plus = (other: Coord): Coord => {
    return new Coord(this.row + other.row, this.col + other.col)
  }
}

export enum Subject {
  Baba = "Baba",
  Key = "Key",
  Water = "Water",
  Column = "Column",
  Wall = "Wall",
  Rock = "Rock",
  Flag = "Flag",
  Bush = "Bush",
  Grass = "Grass",
  Skull = "Skull",
  Lava = "Lava",
  Violet = "Violet",
}

export enum Desc {
  Win = "Win",
  Defeat = "Defeat",
  Float = "Float",
  You = "You",
  Sink = "Sink",
  Stop = "Stop",
  Push = "Push",
  Hot = "Hot",
  Melt = "Melt",
}

export enum Connector {
  Is = "Is",
}

export type Target = Subject | Desc
export const targetIsSubject = (t: Target): t is Subject => {
  return Object.values(Subject).includes(t)
}
export const targetIsDesc = (t: Target): t is Desc => {
  return Object.values(Desc).includes(t)
}

export type Piece = Subject | Word
export const pieceIsSubject = (p: Piece): p is Subject => {
  return Object.values(Subject).includes(p)
}
export const pieceIsWord = (p: Piece): p is Word => {
  return p instanceof Word
}

export type Rule = [Subject, Target]

export type WordType = Subject | Desc | Connector
export class Word {
  public content: WordType

  constructor(content: WordType) {
    this.content = content
  }

  public static isSubject = (content: WordType): content is Subject => {
    return Object.values(Subject).includes(content)
  }

  public static isDesc = (content: WordType): content is Desc => {
    return Object.values(Desc).includes(content)
  }

  public static isConnector = (content): content is Connector => {
    return Object.values(Connector).includes(content)
  }
}

export interface Square {
  word?: Word,
  subjects: Set<Subject>,
}

export class Grid {
  squares: Array<Array<Square>>

  constructor(squares: Array<Array<Square>>) {
    this.squares = squares
  }

  public width = (): number => this.squares[0].length
  public height = (): number => this.squares.length

  public cells = function* (): IterableIterator<[Coord, Square]> {
    for (let row = 0; row < this.height(); row++) {
      for (let col = 0; col < this.width(); col++) {
        yield [new Coord(row, col), this.squares[row][col]]
      }
    }
  }

  public inBounds = (coord: Coord): boolean => {
    return (
      0 <= coord.row &&
      0 <= coord.col &&
      coord.row < this.squares.length &&
      coord.col < this.squares[0].length
    )
  }

  public get = (coord: Coord): Square => {
    if (!this.inBounds(coord)) {
      throw `Can't get square for ${coord} in a grid with ${this.squares.length} rows and ${this.squares[0].length} columns`
    }

    return this.squares[coord.row][coord.col]
  }

  /**
   * All squares between the current point going towards `direction` until
   * and including either an empty space or a boundary
   */
  public lineFrom = (coord: Coord, direction: Move): Array<[Coord, Square?]> => {
    if (!this.inBounds(coord)) {
      throw `Can't get ${direction} line for ${coord} in a grid with ${this.squares.length} rows and ${this.squares[0].length} columns`
    }

    const delta = Coord.fromMove(direction)
    const line = []

    // Walk until out of bounds or empty place
    let current = coord.plus(delta)
    while (this.inBounds(current) && (this.get(current).subjects.size > 0 || this.get(current).word !== null)) {
      line.push([current, this.get(current)])
      current = current.plus(delta)
    }

    // Add last space
    if (this.inBounds(current)) {
      line.push([current, this.get(current)])
    } else {
      line.push([current, null])
    }

    return line
  }

  public withMoves = (changes: Set<[Coord, Subject | Word]>, direction: Move): Grid => {
    const newSquares = this.copySquares()

    // Move everything over
    const delta = Coord.fromMove(direction)

    let changesArray = Array.from(changes)
    changesArray.sort(this.sortAgainstDirection(direction))

    for (let [coord, piece] of changesArray) {
      const oldSquare: Square = newSquares[coord.row][coord.col]
      const newSquare = newSquares[coord.row + delta.row][coord.col + delta.col]

      // Subject
      if (pieceIsSubject(piece)) {
        oldSquare.subjects.delete(piece)
        newSquare.subjects.add(piece)
        // Word
      } else {
        oldSquare.word = null
        newSquare.word = piece
      }
    }

    return new Grid(newSquares)
  }

  sortAgainstDirection = (direction: Move): (a: [Coord, Subject | Word], b: [Coord, Subject | Word]) => number => {
    switch (direction) {
      case Move.Up: return (a, b) => a[0].row - b[0].row
      case Move.Down: return (a, b) => b[0].row - a[0].row
      case Move.Left: return (a, b) => a[0].col - b[0].col
      case Move.Right: return (a, b) => b[0].col - a[0].col
    }
  }

  public withDeletions = (deletions: Set<[Coord, Subject]>): Grid => {
    const newSquares = this.copySquares()

    for (let [coord, subject] of deletions) {
      newSquares[coord.row][coord.col].subjects.delete(subject)
    }

    return new Grid(newSquares)
  }

  public withTransformations = (transformations: Set<[Coord, Subject, Subject]>): Grid => {
    const newSquares = this.copySquares()

    for (let [coord, fromSubject, toSubject] of transformations) {
      newSquares[coord.row][coord.col].subjects.delete(fromSubject)
      newSquares[coord.row][coord.col].subjects.add(toSubject)
    }

    return new Grid(newSquares)
  }

  copySquares = (): Array<Array<Square>> => {
    // Copy existing grid
    const newSquares = new Array(this.squares.length)

    for (let row = 0; row < newSquares.length; row++) {
      newSquares[row] = new Array(this.squares[row].length)
    }

    for (let [coord, square] of this.cells()) {
      newSquares[coord.row][coord.col] = { word: square.word, subjects: new Set([...square.subjects]) }
    }

    return newSquares
  }

  public toString = (): string => {
    const pad = 16
    let output = ""

    let row = 0
    for (let [coord, square] of this.cells()) {
      if (coord.row !== row) {
        row++
        output += '\n'
      }

      let cell = ""

      if (square.word !== null) {
        if (Word.isSubject(square.word.content)) {
          cell += `+${Subject[square.word.content].toUpperCase()}`
        } else if (Word.isDesc(square.word.content)) {
          cell += `+${Desc[square.word.content].toUpperCase()}`
        } else if (Word.isConnector(square.word.content)) {
          cell += `+${Connector[square.word.content].toUpperCase()}`
        }
      }

      for (let subject of square.subjects) {
        cell += `+${Subject[subject]}`
      }

      output += `[${cell.padEnd(10)}]`
    }

    return output
  }
}

// TODO: Add support for "Undo"
export type Action = Move | "Restart"
export type Status = "Win" | "Lose" | "Ongoing"

export class Rules {
  rules: Map<Subject, Set<Target>>

  constructor() {
    this.rules = new Map
  }

  public add = (subject: Subject, target: Target): void => {
    let connections = this.rules.get(subject) || new Set
    connections.add(target)
    this.rules.set(subject, connections)
  }

  public exists = (subject: Subject, target: Target): boolean => {
    return this.targets(subject).has(target)
  }

  public targets = (subject: Subject): Set<Target> => {
    return this.rules.get(subject) || new Set
  }

  public subjectsThatAre = (desc: Desc): Set<Subject> => {
    const subjects = new Set

    // Build set of subjects that you control
    for (let [subject, targets] of this.rules) {
      if (targets.has(desc)) {
        subjects.add(subject)
      }
    }

    return subjects
  }

  // Note: If a subject IS multiple other subjects, an arbitrary one will be
  // chosen
  public subjectToSubjectMap = (): Map<Subject, Subject> => {
    const subjectIsSubject = new Map

    for (let [subject, targets] of this.entries()) {
      for (let target of targets) {
        if (targetIsSubject(target)) {
          subjectIsSubject.set(subject, target)
        }
      }
    }

    return subjectIsSubject
  }

  public entries = (): IterableIterator<[Subject, Set<Target>]> => {
    return this.rules.entries()
  }
}

export class Game {
  grid: Grid
  rules: Rules
  level: Grid

  static neighborDeltas = [
    new Coord(1, 0),
    new Coord(0, 1),
  ]

  constructor(level: Grid, current: Grid, rules: Rules) {
    this.level = level
    this.grid = current
    this.rules = rules
  }

  public static startingAtLevel = (level: Grid): Game => {
    return new Game(level, level, Game.calculateRules(level))
  }

  public withCurrentGrid = (grid: Grid): Game => {
    return new Game(this.level, grid, Game.calculateRules(grid))
  }

  static deletionEffects = (grid: Grid, rules: Rules): Set<[Coord, Subject]> => {
    const sinkSubjects = rules.subjectsThatAre(Desc.Sink)
    const floatSubjects = rules.subjectsThatAre(Desc.Float)

    const youSubjects = rules.subjectsThatAre(Desc.You)
    const defeatSubjects = rules.subjectsThatAre(Desc.Defeat)

    const meltSubjects = rules.subjectsThatAre(Desc.Melt)
    const hotSubjects = rules.subjectsThatAre(Desc.Hot)

    const deletions = new Set

    for (let [coord, square] of grid.cells()) {
      // Sink -> Destroy objects that are not floating (& itself)
      for (let sinkSubject of sinkSubjects) {
        if (square.subjects.has(sinkSubject)) {
          for (let subject of square.subjects) {
            if (!sinkSubjects.has(subject) && !floatSubjects.has(subject)) {
              deletions.add([coord, subject])
              deletions.add([coord, sinkSubject])
            }
          }
        }
      }


      // Defeat -> Destroy objects that belong to you
      for (let defeatSubject of defeatSubjects) {
        if (square.subjects.has(defeatSubject)) {
          for (let subject of square.subjects) {
            if (youSubjects.has(subject)) {
              deletions.add([coord, subject])
            }
          }
        }
      }

      // Hot -> Anything that is MELT is destroyed
      for (let hotSubject of hotSubjects) {
        if (square.subjects.has(hotSubject)) {
          for (let subject of square.subjects) {
            if (meltSubjects.has(subject)) {
              deletions.add([coord, subject])
            }
          }
        }
      }
    }

    return deletions
  }

  static getTransformations = (grid: Grid, rules: Rules): Set<[Coord, Subject, Subject]> => {
    const subjectToSubject = rules.subjectToSubjectMap()
    const transformations = new Set

    for (let [coord, square] of grid.cells()) {
      for (let [subject, target] of subjectToSubject.entries()) {
        if (square.subjects.has(subject)) {
          transformations.add([coord, subject, target])
        }
      }
    }

    return transformations
  }

  public static calculateRules = (grid: Grid): Rules => {
    const rules = new Rules

    for (let [coord, _] of grid.cells()) {
      for (let [subject, target] of Game.rulesFromCoord(grid, coord)) {
        rules.add(subject, target)
      }
    }

    return rules
  }

  static rulesFromCoord = (grid: Grid, coord: Coord): Set<[Subject, Target]> => {
    const word = grid.get(coord).word
    const rules = new Set

    if (word === null) {
      return rules
    }

    // Current square is beginning of rule?
    for (let dCoord of Game.neighborDeltas) {
      const neighbor = coord.plus(dCoord)
      const nextNeighbor = neighbor.plus(dCoord)

      // Neighbors in bounds?
      if (grid.inBounds(neighbor) && grid.inBounds(nextNeighbor)) {
        const connector = grid.get(neighbor).word
        const isConnector = connector !== null && Word.isConnector(connector.content)

        const target = grid.get(nextNeighbor).word
        const isTargetWord = target !== null && (Word.isSubject(target.content) || Word.isDesc(target.content))

        // Makes a rule?
        if (isConnector && isTargetWord) {
          rules.add([word.content, target.content])
        }
      }
    }

    return rules
  }

  public performAction = (action: Action): [Game, boolean] => {
    if (action === "Restart") {
      return [Game.startingAtLevel(this.level), true]
    }

    // Make move if possible
    if (this.status() === "Ongoing") {
      // Move things w/o applying effects
      const moves = this.moveChanges(action)

      if (moves.size === 0) {
        return [this, false]

      } else {
        const gridWithMoves = this.grid.withMoves(moves, action)

        // Apply effects after transition
        const deletions = Game.deletionEffects(gridWithMoves, this.rules)
        const gridWithDeletions = gridWithMoves.withDeletions(deletions)

        // Calculate new rules
        const newRules = Game.calculateRules(gridWithDeletions)

        // Apply object transformations from one subject to another according to rules
        const transformations = Game.getTransformations(gridWithDeletions, newRules)
        const gridWithTransformations = gridWithDeletions.withTransformations(transformations)

        return [new Game(this.level, gridWithTransformations, newRules), true]
      }
    }

    // Don't do anything
    return [this, false]
  }

  // Calculate the effects of moving each individual piece
  moveChanges = (move: Move): Set<[Coord, Subject | Word]> => {
    const changes = new Set

    for (let [coord, subject] of this.yourPieces()) {
      let stagedChanges: Set<[Coord, Subject | Word]> = new Set
      stagedChanges.add([coord, subject])

      for (let [lineCoord, square] of this.grid.lineFrom(coord, move)) {
        const piecesToMove = []
        let squareRole: "Movable" | "Immovable" | "Empty" = "Empty"

        // Level boundary?
        if (square === null) {
          squareRole = "Immovable"
        } else {
          if (square.word !== null) {
            squareRole = "Movable"
            piecesToMove.push(square.word)
          }

          for (let subject of square.subjects) {
            // There's a stop here -> cancel staged changes
            if (this.rules.exists(subject, Desc.Stop)) {
              squareRole = "Immovable"
              break
            }

            // Something here will be displaced even further
            if (this.rules.exists(subject, Desc.Push)) {
              squareRole = "Movable"
              piecesToMove.push(subject)
            }
          }
        }

        // Cancel moves
        if (squareRole === "Immovable") {
          stagedChanges = new Set
          break

          // Move everything until this space
        } else if (squareRole === "Empty") {
          break

          // Continue to the next space
        } else if (squareRole === "Movable") {
          for (let piece of piecesToMove) {
            stagedChanges.add([lineCoord, piece])
          }
        }
      }

      // Copy over pieces from current line
      for (let change of stagedChanges) {
        changes.add(change)
      }
    }

    return changes
  }

  public status = (): Status => {
    let piecesYouControl = 0

    for (let [coord, _] of this.yourPieces()) {
      piecesYouControl++

      // Any pieces on the same square as your piece that are WIN?
      for (let pieceOnSameSquare of this.grid.get(coord).subjects) {
        if (this.rules.exists(pieceOnSameSquare, Desc.Win)) {
          return "Win"
        }
      }
    }

    // You didn't win and have no pieces
    if (piecesYouControl === 0) {
      return "Lose"
    }

    return "Ongoing"
  }

  // Yield all the subjects and where they are that you control
  yourPieces = function* (): IterableIterator<[Coord, Subject]> {
    const subjectsYouControl = this.rules.subjectsThatAre(Desc.You)

    for (let [coord, square] of this.grid.cells()) {
      for (let subject of square.subjects) {
        if (subjectsYouControl.has(subject)) {
          yield [coord, subject]
        }
      }
    }
  }
}